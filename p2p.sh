ip netns add red
ip netns add blue

ip link add red-blue type veth peer name blue-red

ip link set red-blue netns red
ip link set blue-red netns blue

ip netns exec red ip link set red-blue up
ip netns exec blue ip link set blue-red up

ip netns exec red ip address add 10.0.0.1/24 dev red-blue
ip netns exec blue ip address add 10.0.0.2/24 dev blue-red